# Helm install image

Minimal images with [Helm](https://helm.sh/) and [kubectl](https://v1-15.docs.kubernetes.io/docs/tasks/tools/install-kubectl/) installed. Built for GitLab's cluster integration.

There are three variants:

## Helm 2

Contains Helm 2 and kubectl

### Binaries

- Helm 2:
  - `/usr/bin/helm`
  - `/usr/bin/tiller`
- kubectl:
  - `/usr/bin/kubectl`

### Image

```plaintext
registry.gitlab.com/gitlab-org/cluster-integration/helm-install-image/releases/2.17.0-kube-1.16.15-alpine-3.12
```

## Helm 3

Contains Helm 3 and kubectl

### Binaries

- Helm 3:
  - `/usr/bin/helm`
- kubectl:
  - `/usr/bin/kubectl`

### Image

```plaintext
registry.gitlab.com/gitlab-org/cluster-integration/helm-install-image/releases/3.5.3-kube-1.16.15-alpine-3.12
```

## Helm 2to3

Contains both Helm 2 and Helm 3, as well as the `helm-2to3` plugin installed on Helm 3 to facilitate Helm 3 migrations.

### Binaries

- Helm 2:
  - `/usr/bin/helm2`
  - `/usr/bin/tiller`
- Helm 3:
  - `/usr/bin/helm3`
- kubectl:
  - `/usr/bin/kubectl`

### Image

```plaintext
registry.gitlab.com/gitlab-org/cluster-integration/helm-install-image/releases/helm-2to3-2.17.0-3.5.3-kube-1.16.15-alpine-3.12
```
